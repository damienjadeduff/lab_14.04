
#!/bin/sh

# set TR keyboard as default in system tray
# set default timezone to Istanbul

LOCAL=/usr/local

UPDATE='apt-get update'
INSTALL='apt-get -y install --no-install-recommends'
DELETE='apt-get purge'
CLEAN='apt-get clean'
DPKG='dpkg -i'

$UPDATE

# lubuntu

PACKAGES="lxde lubuntu-core xterm lubuntu-default-session language-selector-gnome lxlauncher lxtask xfce4-notifyd xfce4-power-manager network-manager-gnome"
$INSTALL $PACKAGES

### General

PACKAGES="software-properties-common patch vim tree unrar dos2unix wget curl"
$INSTALL $PACKAGES

PACKAGES="fonts-droid fonts-cabin xfonts-75dpi xfonts-100dpi"
$INSTALL $PACKAGES

PACKAGES="geany geany-plugin-treebrowser"
$INSTALL $PACKAGES

PACKAGES="nautilus python-gpgme"
$INSTALL $PACKAGES

# PACKAGES="dropbox*deb"
# $DPKG $PACKAGES

### Introduction to Information Systems

PACKAGES="lynx firefox thunderbird"
$INSTALL $PACKAGES

# PACKAGES="madedit*deb"
# $DPKG $PACKAGES

PACKAGES="libreoffice-writer libreoffice-calc libreoffice-draw libreoffice-impress libreoffice-math"
$INSTALL $PACKAGES

PACKAGES="abiword* gnumeric*"
$DELETE $PACKAGES

PACKAGES="imagemagick gimp inkscape"
$INSTALL $PACKAGES

PACKAGES="python3 python3-bottle python3-tz python3-imaging python3-pandas python3-matplotlib"
$INSTALL $PACKAGES

PACKAGES="ipython3 ipython3-notebook ipython3-qtconsole"
$INSTALL $PACKAGES

### Programming

PACKAGES="g++ manpages-dev make gdb valgrind codeblocks"
$INSTALL $PACKAGES

### LaTeX

PACKAGES="texlive texlive-generic-recommended texlive-lang-european geany-plugin-latex"
$INSTALL $PACKAGES

### Database Systems

PACKAGES="sqlite3 mercurial git python3-sphinx"
$INSTALL $PACKAGES

### System Programming

PACKAGES="nasm strace ltrace libfuse-dev"
$INSTALL $PACKAGES
##+ linux-headers-$(uname -r)

### Functional Programming

PACKAGES="ghc libghc-quickcheck2-dev"
$INSTALL $PACKAGES

$CLEAN

### Online Python Tutor

PACKAGES="python-bottle"
$INSTALL $PACKAGES

# # CURDIR=$PWD
# #
# # mkdir -p $LOCAL/share
# # cd $LOCAL/share
# # git clone https://github.com/pgbovine/OnlinePythonTutor.git
# #
# # cd OnlinePythonTutor
# # rm -rf .git*
# # rm -rf v1-v2
# # mv README README.1ST
# # mv v3/* .
# # rmdir v3
# #
# # mkdir -p $LOCAL/bin
# # cd $LOCAL/bin
# # echo "#!/bin/sh" > OnlinePythonTutor
# # echo "cd $LOCAL/share/OnlinePythonTutor" >> OnlinePythonTutor
# # echo "python2 bottle_server.py" >> OnlinePythonTutor
# # chmod a+rx OnlinePythonTutor
# #
# # cd $CURDIR

### Interactive

apt-get install lubuntu-restricted-extras

PACKAGES="ttf-mscorefonts-installer"
$INSTALL $PACKAGES

### Settings

# activate geany-plugin-treebrowser
# firefox: set home page to Ninova
# firefox plugin: FireFTP, add button
# firefox: add bookmark for Online Python Tutor

echo "----------------------------------------------"
echo "----------------------------------------------"
echo BIL103E install finished.
echo "----------------------------------------------"
echo "----------------------------------------------"
