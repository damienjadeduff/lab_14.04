#!/bin/bash

echo WARNING: Script may ask for sudo password
sleep 2

echo ++++++++++++++++++ 
echo ++++++++++++++++++ Let us update everything
echo ++++++++++++++++++ 
sudo apt-get -f install 
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade
sudo apt-get -y install elinks

echo ++++++++++++++++++ 
echo ++++++++++++++++++ Now let us go to a temporary directory and download madedit to there 
echo ++++++++++++++++++ 
mkdir ~/tmp 
cd ~/tmp
rm madedit.deb
wget -O madedit.deb http://sourceforge.net/projects/madedit/files/MadEdit/MadEdit-0.2.9/madedit_0.2.9-1_amd64.deb/download?use_mirror=netcologne\&download=\&failedmirror=dfn.dl.sourceforge.net

echo ++++++++++++++++++ 
echo ++++++++++++++++++ Now let us install madedit -- all dependencies probably already exist
echo ++++++++++++++++++ 
sudo dpkg --install madedit.deb 
